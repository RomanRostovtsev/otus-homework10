import './App.css';
import axios from 'axios';
import { useState } from 'react';



function App() {
    const [forecasts, setForecasts] = useState([]);


    return (
    <div className="App">
          <table>
              <thead>
                  <tr>
                      <th>Date</th>
                      <th>Temp. (C)</th>
                      <th>Temp. (F)</th>
                      <th>Summary</th>
                  </tr>
              </thead>
              <tbody>
                  {forecasts.map(forecast =>
                      <tr key={forecast.date}>
                          <td>{forecast.date}</td>
                          <td>{forecast.temperatureC}</td>
                          <td>{forecast.temperatureF}</td>
                          <td>{forecast.summary}</td>
                      </tr>
                  )}
              </tbody>
          </table>
          <br></br>
            <button onClick={() => {
                let uri = `${process.env.REACT_APP_BACKEND_URL || ''}/WeatherForecast`;
                axios.get(uri).then(response => {
                    setForecasts(response.data);
                }).catch(error => {
                    let msg;
                    if (error.response) {
                        msg = error.response.status;
                    } else {
                        msg = error.message;
                    }
                    alert("Could not get a forecast: " + msg);
                })

            }} >Get forecast</button>
    </div>
  );
}

export default App;
