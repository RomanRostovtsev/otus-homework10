namespace WebAPI
{
    public class Program
    {
        private const string CorsPolicy = "CorsPolicy";

        public static void Main(string[] args)
        {
            IConfiguration config = new ConfigurationBuilder()
                                    .AddJsonFile("appsettings.json")
                                    .AddEnvironmentVariables()
                                    .Build();

            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.

            builder.Services.AddControllers();
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();
            builder.Services.AddCors((corsOptions) =>
            {
                corsOptions.AddPolicy(CorsPolicy, builder =>
                {
                    builder.WithOrigins(config.GetSection("CORS:Origins").Get<string[]>())
                           .WithHeaders(config.GetSection("CORS:Headers").Get<string[]>())
                           .WithMethods(config.GetSection("CORS:Methods").Get<string[]>());
                });
            });

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseHttpsRedirection();
            app.UseCors(CorsPolicy);
            app.UseAuthorization();


            app.MapControllers();

            app.Run();
        }
    }
}